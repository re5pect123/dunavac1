$(function() {
	// BANNER
	var bannercorrente=0;
	if(window.innerWidth<=414){
		var banners= [
			'https://static.inter.it/media/abbonamenti/images/new/Visore_SoldOut_Mobile.jpg'
		];
	}else if(window.innerWidth<=768){
		var banners= [
			'https://static.inter.it/media/abbonamenti/images/new/Visore_SoldOut_Tablet.jpg'
		];
	}else{
		var banners= [
			'https://static.inter.it/media/abbonamenti/images/new/Visore_SoldOut_Desktop.jpg'
		];
	}
	$('#banner').attr('src',banners[bannercorrente]);
	function smoothScroll(target) {
		$('body,html').animate(	{'scrollTop':target.offset().top}, 600 );
	}
	// FAQ
	$(".abbonamenti-faq").click(function(){
		var cosa=$(this);
		var apro=$(this).attr("id");
		$(".abbonamenti-faq").each(function(){
			$(this).removeClass("abbonamenti-faq-selected");
		});
		cosa.addClass("abbonamenti-faq-selected");
		$(".zona-faqs .abbonamenti-faq-domande").hide("fast");
		$(".zona-faqs .abbonamenti-richiamami").hide("fast");
		$(".zona-faqs .abbonamenti-informazioni").hide("fast");
		$("."+apro).show("fast");
		$("html,body").animate({ scrollTop: $("."+apro).offset().top }, "slow");
		return false;
	});
	$.ajax({
		method: "POST",
		ajax:true,
		url: "https://tools.inter.it/open/abbonamenti/dammi_faqs.php"
	}).done(function( msg ) {
		if(msg=="no"){
			alert("Errore.");
			return false;
		}else if(msg=="errore"){
			alert("Si è verificato un errore, riprova.");
			return false;
		}else{
			$(".abbonamenti-faq-categoria").html(msg);
			$(".abbonamenti-faq-domanda").html("");
			$(".abbonamenti-faq-risposta").html("");
			$(".abbonamenti-faq-risposta").hide("fast");
			$(".faq-voto").hide("fast");
			$("#faq_categoria").change(function(){
				if($(this).val()!=""){
					$.ajax({
						method: "POST",
						ajax:true,
						url: "https://tools.inter.it/open/abbonamenti/dammi_domande.php",
						data:{
							id:$(this).val()
						}
					}).done(function( msg ) {
						if(msg=="no"){
							alert("Errore.");
							return false;
						}else if(msg=="errore"){
							alert("Si è verificato un errore, riprova.");
							return false;
						}else{
							$("html,body").animate({ scrollTop: $(".abbonamenti-contatta").offset().top }, "slow");
							$(".abbonamenti-faq-domanda").html(msg);
							$(".abbonamenti-faq-risposta").html("");
							$(".abbonamenti-faq-risposta").hide("fast");
							$(".faq-voto").hide("fast");
							$("#faq_domande").change(function(){
								var risposta=$(this).val();
								if(risposta!=""){
									$.ajax({
										method: "POST",
										ajax:true,
										url: "https://tools.inter.it/open/abbonamenti/dammi_risposta.php",
										data:{
											id:risposta
										}
									}).done(function( msg ) {
										if(msg=="no"){
											alert("Errore.");
											return false;
										}else if(msg=="errore"){
											alert("Si è verificato un errore, riprova.");
											return false;
										}else{
											$("html,body").animate({ scrollTop: $(".abbonamenti-contatta").offset().top }, "slow");
											$(".abbonamenti-faq-risposta").css("display","inline-table");
											$(".faq-voto").css("display","block");
											$(".si").attr("id","si_"+risposta);
											$(".no").attr("id","no_"+risposta);
											$(".abbonamenti-faq-risposta").html(msg);
											$(".votazione").click(function(){
												$.ajax({
													method: "POST",
													ajax:true,
													url: "https://tools.inter.it/open/abbonamenti/vota_risposta.php",
													data:{
														voto:$(this).attr("id")
													}
												}).done(function( msg ) {
													if(msg=="not"){
														alert("Errore.");
														return false;
													}else if(msg=="errore"){
														alert("Si è verificato un errore, riprova.");
														return false;
													}else{
														if(msg=="si"){
															ga('intjer.send', 'event', 'LP_Campagna_abbonamenti_2018', 'click', 'SI');
															alert("Grazie della risposta!");
															$(".abbonamenti-faq-domanda").html("");
															$(".abbonamenti-faq-risposta").html("");
															$(".abbonamenti-faq-risposta").hide("fast");
															$(".faq-voto").hide("fast");
															$("#faq_categoria").val("");
														}else{
															ga('intjer.send', 'event', 'LP_Campagna_abbonamenti_2018', 'click', 'No');
															$(".abbonamenti-faq-risposta").html("<div class='clear'></div><div class='immaginina_faq'></div><iframe onload='resizeIframe(this);' style='width:100%;' srcdoc=\"<!DOCTYPE html><html lang=''><head><title>Web to Case ST18_faq</title><meta charset='utf-8' /><meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'> <!--[if IE]><link rel='stylesheet' type='text/css' media='all' href='https://www.formstack.com/forms/css/3/ie.css?20140508' /> <![endif]--> <!--[if IE 7]><link rel='stylesheet' type='text/css' media='all' href='https://www.formstack.com/forms/css/3/ie7.css' /><![endif]--> <!--[if IE 6]><link rel='stylesheet' type='text/css' media='all' href='https://www.formstack.com/forms/css/3/ie6fixes.css' /><![endif]--><link rel='stylesheet' type='text/css' href='//static.formstack.com/forms/css/3/reset.css' /><link rel='stylesheet' type='text/css' href='//static.formstack.com/forms/css/3/jquery-ui.css' /><link rel='stylesheet' type='text/css' href='//static.formstack.com/forms/css/3/default-v4.css' /><link rel='stylesheet' type='text/css' href='//static.formstack.com/forms/css/3/light.css' /><link rel='stylesheet' type='text/css' href='//static.formstack.com/forms/../common/css/uil-static.css' /><link rel='stylesheet' type='text/css' href='//static.formstack.com/forms/css/common/dialogs.css' /><style type='text/css'>#fsLocal{margin:0;padding:0;background:none;}.fsBody .fsForm{margin:0 !important;width:100%;background:none;}</style></head><body class='fsBody' id='fsLocal'><form target='_top' method='post' novalidate enctype='multipart/form-data' action='https://www.formstack.com/forms/index.php' class='fsForm fsSingleColumn fsMaxCol1' id='fsForm3054430'> <input type='hidden' name='form' value='3054430' /> <input type='hidden' name='viewkey' value='y5dF7UXdfr' /> <input type='hidden' name='hidden_fields' id='hidden_fields3054430' value='' /> <input type='hidden' name='_submit' value='1' /> <input type='hidden' name='incomplete' id='incomplete3054430' value='' /> <input type='hidden' name='incomplete_password' id='incomplete_password3054430' /> <input type='hidden' name='style_version' value='3' /> <input type='hidden' id='viewparam' name='viewparam' value='667822' /><div id='requiredFieldsError' style='display:none;'>Per favore inserisci un valore valido in ogni campo richiesto.</div><div id='invalidFormatError' style='display:none;'>Si prega di assicurarsi che tutti i valori siano in formato corretto.</div><div id='resumeConfirm' style='display:none;'>Sei sicuro di voler abbandonare questo modulo e di volerlo recuperare più tardi?</div><div id='resumeConfirmPassword' style='display: none;'>Sei sicuro di volere lasciare questo modulo e riprendere più tardi? Se sì, per salvare in modo sicuro il modulo inserire una password di seguito.</div><div id='saveAndResume' style='display: none;'>Salva e riprendi più tardi</div><div id='saveResumeProcess' style='display: none;'>Salva e ottieni il link</div><div id='fileTypeAlert' style='display:none;'>Per il campo selezionato devi caricare uno dei seguenti tipi di file:</div><div id='embedError' style='display:none;'>C&#039;è stato un errore visualizzando il formulario. Per favore copia e incolla di nuovo il codice di inserimento.</div><div id='applyDiscountButton' style='display:none;'>Applicare sconto</div><div id='dcmYouSaved' style='display:none;'>Lei ha salvato</div><div id='dcmWithCode' style='display:none;'>con il codice</div><div id='submitButtonText' style='display:none;'>Invia</div><div id='submittingText' style='display:none;'>Invio</div><div id='validatingText' style='display:none;'>Convalida</div><div id='autocaptureDisabledText' style='display:none;'></div><div id='paymentInitError' style='display:none;'>Si è verificato un errore durante l&#039;inizializzazione del sistema di pagamento in questo modulo. Si prega di contattare il proprietario del modulo per correggere questo problema.</div><div id='checkFieldPrompt' style='display:none;'>Si prega di verificare sul campo:</div><div class='fsPage' id='fsPage3054430-1'><div class='fsSection fs1Col'><div id='fsRow3054430-1' class='fsRow fsFieldRow fsLastRow fsHidden'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsHidden fsSpan100 fsHiddenByFieldSetting' id='fsCell64066594' lang='it' fs-field-type='text'> <label id='label64066594' class='fsLabel' for='field64066594'>Channel </label> <input type='text' id='field64066594' name='field64066594' size='50' value='Landing page | FAQ' class='fsField' /></div></div><div id='fsRow3054430-2' class='fsRow fsFieldRow fsLastRow fsHidden'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsHidden fsSpan100 fsHiddenByFieldSetting' id='fsCell64066577' lang='it' fs-field-type='text'> <label id='label64066577' class='fsLabel' for='field64066577'>Case Origin </label> <input type='text' id='field64066577' name='field64066577' size='50' value='Web' class='fsField' /></div></div><div id='fsRow3054430-3' class='fsRow fsFieldRow fsLastRow fsHidden'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsHidden fsSpan100 fsHiddenByFieldSetting' id='fsCell64066578' lang='it' fs-field-type='text'> <label id='label64066578' class='fsLabel' for='field64066578'>Campaign ID </label> <input type='text' id='field64066578' name='field64066578' size='50' value='7011o000000ltykAAA' class='fsField' /></div></div><div id='fsRow3054430-4' class='fsRow fsFieldRow fsLastRow'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100' id='fsCell64066579' lang='it' fs-field-type='text'> <label id='label64066579' class='fsLabel fsRequiredLabel' for='field64066579'>NOME<span class='fsRequiredMarker'>*</span> </label> <input type='text' id='field64066579' name='field64066579' size='20' required value='' class='fsField fsRequired' aria-required='true' /></div></div><div id='fsRow3054430-5' class='fsRow fsFieldRow fsLastRow'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100' id='fsCell64066580' lang='it' fs-field-type='text'> <label id='label64066580' class='fsLabel fsRequiredLabel' for='field64066580'>COGNOME<span class='fsRequiredMarker'>*</span> </label> <input type='text' id='field64066580' name='field64066580' size='20' required value='' class='fsField fsRequired' aria-required='true' /></div></div><div id='fsRow3054430-6' class='fsRow fsFieldRow fsLastRow'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100' id='fsCell64066581' lang='it' fs-field-type='email'> <label id='label64066581' class='fsLabel fsRequiredLabel' for='field64066581'>INDIRIZZO EMAIL<span class='fsRequiredMarker'>*</span> </label> <input type='email' id='field64066581' name='field64066581' size='50' required='required' value='' class='fsField fsFormatEmail fsRequired' aria-required='true' /></div></div><div id='fsRow3054430-7' class='fsRow fsFieldRow fsLastRow'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100' id='fsCell64066582' lang='it' fs-field-type='phone'> <label id='label64066582' class='fsLabel' for='field64066582'>TELEFONO </label> <input type='tel' id='field64066582' name='field64066582' size='20' value='' class='fsField fsFormatPhoneIT ' data-country='IT' data-format='user' /></div></div><div id='fsRow3054430-8' class='fsRow fsFieldRow fsLastRow'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100' id='fsCell64066583' lang='it' fs-field-type='datetime'><fieldset id='label64066583'><legend class='fsLabel fsRequiredLabel fsLabelVertical'><span>DATA DI NASCITA<span class='fsRequiredMarker'>*</span></span></legend><div class='fieldset-content'><span style='display:none;' id='fsCalendar64066583ImageUrl'>https://www.formstack.com/forms/images/2/calendar.png</span> <input data-skip-validation data-date-format='d/m/YY' type='hidden' id='field64066583Format' name='field64066583Format' value='DMY' /><div class='hidden'><label for='field64066583D'>Giorno</label></div> <select id='field64066583D' name='field64066583D' class=' fsField fsRequired' aria-required='true'><option value=''></option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option> </select><div class='hidden'><label for='field64066583M'>Mese</label></div> <select id='field64066583M' name='field64066583M' class=' fsField fsRequired' aria-required='true'><option value=''></option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option> </select><div class='hidden'><label for='field64066583Y'>Anno</label></div> <select id='field64066583Y' name='field64066583Y' class=' fsField fsRequired' aria-required='true'><option value=''></option><option value='1938'>1938</option><option value='1939'>1939</option><option value='1940'>1940</option><option value='1941'>1941</option><option value='1942'>1942</option><option value='1943'>1943</option><option value='1944'>1944</option><option value='1945'>1945</option><option value='1946'>1946</option><option value='1947'>1947</option><option value='1948'>1948</option><option value='1949'>1949</option><option value='1950'>1950</option><option value='1951'>1951</option><option value='1952'>1952</option><option value='1953'>1953</option><option value='1954'>1954</option><option value='1955'>1955</option><option value='1956'>1956</option><option value='1957'>1957</option><option value='1958'>1958</option><option value='1959'>1959</option><option value='1960'>1960</option><option value='1961'>1961</option><option value='1962'>1962</option><option value='1963'>1963</option><option value='1964'>1964</option><option value='1965'>1965</option><option value='1966'>1966</option><option value='1967'>1967</option><option value='1968'>1968</option><option value='1969'>1969</option><option value='1970'>1970</option><option value='1971'>1971</option><option value='1972'>1972</option><option value='1973'>1973</option><option value='1974'>1974</option><option value='1975'>1975</option><option value='1976'>1976</option><option value='1977'>1977</option><option value='1978'>1978</option><option value='1979'>1979</option><option value='1980'>1980</option><option value='1981'>1981</option><option value='1982'>1982</option><option value='1983'>1983</option><option value='1984'>1984</option><option value='1985'>1985</option><option value='1986'>1986</option><option value='1987'>1987</option><option value='1988'>1988</option><option value='1989'>1989</option><option value='1990'>1990</option><option value='1991'>1991</option><option value='1992'>1992</option><option value='1993'>1993</option><option value='1994'>1994</option><option value='1995'>1995</option><option value='1996'>1996</option><option value='1997'>1997</option><option value='1998'>1998</option><option value='1999'>1999</option><option value='2000'>2000</option><option value='2001'>2001</option><option value='2002'>2002</option><option value='2003'>2003</option><option value='2004'>2004</option><option value='2005'>2005</option><option value='2006'>2006</option><option value='2007'>2007</option><option value='2008'>2008</option><option value='2009'>2009</option><option value='2010'>2010</option><option value='2011'>2011</option><option value='2012'>2012</option><option value='2013'>2013</option><option value='2014'>2014</option><option value='2015'>2015</option><option value='2016'>2016</option><option value='2017'>2017</option><option value='2018'>2018</option> </select> <input data-skip-validation type='text' id='fsCalendar64066583Link' class='fsCalendarPickerLink' style='display:none;' aria-hidden='true' /><div id='fsCalendar64066583' class='fsCalendar' style=' position:absolute'></div></div></fieldset></div></div><div id='fsRow3054430-9' class='fsRow fsFieldRow fsLastRow'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100' id='fsCell64066584' lang='it' fs-field-type='select'> <label id='label64066584' class='fsLabel fsRequiredLabel' for='field64066584'>Motivo del contatto<span class='fsRequiredMarker'>*</span> </label> <select id='field64066584' name='field64066584' size='1'required class='fsField fsRequired' aria-required='true'><option value='---'>---</option><option value='ABBONAMENTI'>ABBONAMENTI</option><option value='TESSERA SIAMO NOI'>TESSERA SIAMO NOI</option> </select></div></div><div id='fsRow3054430-10' class='fsRow fsFieldRow fsLastRow'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100' id='fsCell64066587' lang='it' fs-field-type='textarea'> <label id='label64066587' class='fsLabel' for='field64066587'>MESSAGGIO </label><textarea id='field64066587' class='fsField' name='field64066587' rows='10' cols='50' ></textarea></div></div><div id='fsRow3054430-11' class='fsRow fsFieldRow fsLastRow'><div class='fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100' id='fsCell64070964' lang='it' fs-field-type='richtext'><p><span style='font-size: 11px; line-heigh:13px'>Per maggiori informazioni relative al trattamento dei dati personali da parte di F.C. Internazionale Milano S.p.A. si prenda visione dell'<a href='//www.inter.it/aas/link?id=619&v=1' target='_blank'>INFORMATIVA PRIVACY AI SENSI DEL REGOLAMENTO UE 679/2016</a>.</span></p></div></div></div></div><div id='fsSubmit3054430' class='fsSubmit fsPagination'> <button type='button' id='fsPreviousButton3054430' class='fsPreviousButton' value='Previous Page' style='display:none;' aria-label='Precedente'><span class='fsFull'>Precedente</span><span class='fsSlim'>&larr;</span></button> <button type='button' id='fsNextButton3054430' class='fsNextButton' value='Next Page' style='display:none;' aria-label='Successivo'><span class='fsFull'>Successivo</span><span class='fsSlim'>&rarr;</span></button> <input id='fsSubmitButton3054430' class='fsSubmitButton' style='display: block;' type='submit' value='Invia' /><div class='clear'></div></div> <script type='text/javascript' src='//static.formstack.com/forms/js/3/jquery.min.js'></script> <script type='text/javascript' src='//static.formstack.com/forms/js/3/jquery-ui.min.js'></script> <script type='text/javascript' src='//static.formstack.com/forms/js/3/scripts.js'></script> <script type='text/javascript' src='//static.formstack.com/forms/js/3/analytics.js'></script> <script type='text/javascript' src='//static.formstack.com/forms/js/3/google-phone-lib.js'></script> <script type='text/javascript'>(function(){if(typeof sessionStorage!=='undefined'&&sessionStorage.fsFonts){document.documentElement.className=document.documentElement.className+=' wf-active';} var pre=document.createElement('link');pre.rel='preconnect';pre.href='https://fonts.googleapis.com/';pre.setAttribute('crossorigin','');var s=document.getElementsByTagName('head')[0];s.appendChild(pre);var fontConfig={google:{families:['Lato:400,700']},timeout:2000,active:function(){if(typeof sessionStorage==='undefined'){return;} sessionStorage.fsFonts=true;}};if(typeof WebFont==='undefined'){window.WebFontConfig=fontConfig;var wf=document.createElement('script');wf.type='text/javascript';wf.async='true';wf.src=('https:'==document.location.protocol?'https':'http')+'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';s.appendChild(wf);}else{WebFont.load(fontConfig);}})();if(window.addEventListener){window.addEventListener('load',loadFormstack,false);}else if(window.attachEvent){window.attachEvent('onload',loadFormstack);}else{loadFormstack();} function loadFormstack(){var form3054430=new Formstack.Form(3054430,'https://www.formstack.com/forms/');form3054430.logicFields=[];form3054430.calcFields=[];form3054430.dateCalcFields=[];form3054430.init();form3054430.plugins.analytics=new Formstack.Analytics('https://www.formstack.com',3054430,form3054430);form3054430.plugins.analytics.trackTouch();form3054430.plugins.analytics.trackBottleneck();window.form3054430=form3054430;};</script> </form> <img src='https://www.formstack.com/forms/count.php?3054430' alt='Form View Counter' width='1' height='1' border='0' /><div class='fs-ngdialog fs-modal-medium fs-form-dialog fs-form-dialog--hidden'><div class='fs-ngdialog-overlay'></div><div class='fs-ngdialog-content'><div class='fs-modal__top fs-form-dialog__title'></div><div class='fs-modal__middle'><div class='fs-form-dialog__message'></div> <label class='hidden' for='fsSaveResumePassword'>Indicare il salvataggio e riprendere la password</label> <input type='password' id='fsSaveResumePassword' class='fs-form-input fs-form-dialog__password fs-form-dialog--hidden fs--mt20' placeholder='Indicare il salvataggio e riprendere la password'></div><div class='fs-modal__bottom'> <a class='fs-form-dialog__cancel fs-btn2 fs-btn2--size_medium fs-btn2--style_edit-dark' title='Cancella'><div class='fs-btn2__content'><div class='fs-btn2__text'> <span class='fs-form-dialog__button-text'>Cancella</span></div></div> </a> <a class='fs-form-dialog__confirm fs-btn2 fs-btn2--size_medium fs-btn2--style_create fs--float-right' title='Confermare'><div class='fs-btn2__content'><div class='fs-btn2__text'> <span class='fs-form-dialog__button-text'>Confermare</span></div></div> </a></div></div></div></body></html>\" frameborder='0'></iframe>");
															$(".faq-voto").hide("fast");
														}
														$("html,body").animate({ scrollTop: $(".abbonamenti-contatta").offset().top }, "slow");
														return false;
													}
													return false;
												});
												return false;
											});
										}
										return false;
									});
								}else{
									$(".abbonamenti-faq-risposta").html("");
									$(".abbonamenti-faq-risposta").hide("fast");
									$(".faq-voto").hide("fast");
									return false;
								}
							});
							return false;
						}
						return false;
					});
				}else{
					$(".abbonamenti-faq-domanda").html("");
					$(".abbonamenti-faq-risposta").html("");
					$(".abbonamenti-faq-risposta").hide("fast");
					$(".faq-voto").hide("fast");
					return false;
				}
			});
			return false;
		}
		return false;
	});
});
function resizeIframe(obj,apro) {
	obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
	if(apro=="abbonamenti-richiamami"){
		$("."+apro).hide("fast");
	}
}