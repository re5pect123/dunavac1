/*
var $ = require('jquery');
var Slick = require('slick-carousel');
var Shopcarousel = (function() {

	'use strict';

	function Shopcarousel() {
		this.init();
	}

	Shopcarousel.prototype.init = function() {
		console.log('new Shopcarousel initialiazed');
	};

	Shopcarousel.prototype.write = function(val) {
		console.log('Shopcarousel method --> .write(\'val\')',val);
	};

	return Shopcarousel;

})();

module.exports = Shopcarousel;
*/

/* auto init modulo */
$(document).ready(function () {
	$('.js-shopcarousel').each(function(index,el) {
		$(el).slick({
			infinite: true,
			slidesToShow: parseInt($(this).data('shopcarouselitems')),
			slidesToScroll: 1
		});
	});
});