let Utilities = {

    const: COOKIE_TERRITORY_VENDOR_INDEX = 6,

    getUserCountry: function(){
        let deferred = $.Deferred();

        let country = Cookies.get('territory');
        if(!country || country === 'undefined'){

            if (window.__cmp){
                window.__cmp('addEventListener', 'cmpReady', function () {
                    getVendorConsent().then(function (consents) {

                        let hasTerritoryCookieConsent = consents && consents[COOKIE_TERRITORY_VENDOR_INDEX];
                        if(hasTerritoryCookieConsent){
                            $.get("https://static.inter.it/aas/cc")
                                .success(function(territory) {
                                    if(territory){
                                        Cookies.set('territory', territory, {expires: 365});
                                        deferred.resolve(territory);
                                    }else{
                                        deferred.resolve(undefined);
                                    }
                                })
                                .error(function () {
                                    deferred.resolve(undefined);
                                });
                        }else{
                            deferred.resolve(undefined);
                        }

                    });
                });
            }else{
                deferred.resolve(undefined);
            }

        }else{
            deferred.resolve(country);
        }

        return deferred.promise();
    },

    isChineseUser: function(){
        let deferred = $.Deferred();

        this.getUserCountry()
            .then(function (territory) {
                if(territory === "CN"){
                    deferred.resolve(true);
                }else{
                    deferred.resolve(false);
                }
            });

        return deferred.promise();
    }
};