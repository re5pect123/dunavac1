var HeroBanner = {
    //component specific functions here
}

//auto refresh page every 60 seconds if a match is currently being played
$( document ).ready(function() {
    
    var isMatchBeingPlayed = $("#is-match-being-played").val();
    if (isMatchBeingPlayed == 'true') {
        setInterval(function () {
            window.location.reload();
        }, 60000);
    }
    
});