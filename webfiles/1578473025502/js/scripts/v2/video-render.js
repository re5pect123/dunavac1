$(document).ready(function() {
    Utilities.isChineseUser().then(function (isChineseUser) {
        if(isChineseUser){
            $('.all-countries-video').hide();
            $('.chinese-country-video').show();
        }else{
            $('.chinese-country-video').hide();
            $('.all-countries-video').show();
        }
    });
});