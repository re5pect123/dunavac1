var PartnerList = {
        /**
         * Populates the sponsors depending on the current territory
         * @param territory
         */
    populateSponsors:  function (territory) {
        var contextPath = $("#context-path").val();
        if(contextPath && contextPath[contextPath.length -1] == "/"){
            contextPath = contextPath.slice(0, -1);
        }

        $.ajax({
            type: "GET",
            dataType: "json",
            url: contextPath + "/restservices/partners/" + territory
        }).then(function (data) {
            $.each(data, function (i, partner) {
                var rowNumber = partner.footerRowNumber;
                var rowId = 'partner-list-row-' + rowNumber;
                var ul = null;
                if (!$("#" + rowId).length) {
                    if (ul == null) {
                        var divContainer = $('#partner-list-container');
                        ul = document.createElement("ul");
                        ul.setAttribute("id", rowId);
                        $(divContainer).append(ul);
                    }
                } else {
                    ul = $("#" + rowId);
                }

                var li = document.createElement("li");
                var sponsorBlock = document.createElement("a");

                sponsorBlock.setAttribute("style", "background-image: url(" + partner.logoURL + "); width: " + partner.width + "px; height: " + partner.height + "px");
                sponsorBlock.setAttribute("class", "main-footer-sponsor__item");
                sponsorBlock.setAttribute("title", partner.name);
                sponsorBlock.setAttribute("href", partner.url);
                sponsorBlock.setAttribute("target", "_blank");

                var sponsorNameContainer = document.createElement("span");
                var sponsorName = document.createTextNode(partner.name);
                sponsorNameContainer.append(sponsorName);

                sponsorBlock.appendChild(sponsorNameContainer);
                li.append(sponsorBlock);
                ul.append(li);
            });
        });
    }
}

$( document ).ready(function() {
    var url = new URL(location.href);
    var forcedTerritory = url.searchParams.get("territory");

    if (forcedTerritory != null) {
        return;
    }

    Utilities.getUserCountry().then(function (territory) {
        if(territory){
            PartnerList.populateSponsors(territory);
        }
    });
});

//module.exports = PartnerList;