fos.Router.setData({
    "base_url": "",
    "routes": {
        "abbonamenti.welcomekit2017check.ajax": {
            "tokens": [["text", "\/pp\/ajax\/abbonamenti\/welcomekit2017check"]],
            "defaults": [],
            "requirements": {"_method": "GET|POST"},
            "hosttokens": []
        },
        "abbonamenti.welcomekit2017save.ajax": {
            "tokens": [["text", "\/pp\/ajax\/abbonamenti\/welcomekit2017save"]],
            "defaults": [],
            "requirements": {"_method": "GET|POST"},
            "hosttokens": []
        },
        "abbonamenti.welcomekit.ajax": {
            "tokens": [["text", "\/pp\/ajax\/abbonamenti\/welcomekit"]],
            "defaults": [],
            "requirements": {"_method": "GET|POST"},
            "hosttokens": []
        },
        "abbonamenti.get_page.ajax": {
            "tokens": [["text", "\/pp\/ajax\/abbonamenti\/page"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "allenatore_get_header_stagioni": {
            "tokens": [["text", "\/pp\/ajax\/squdra\/allenatore\/header\/stagioni"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "allenatore_get_statistiche_stagione": {
            "tokens": [["text", "\/pp\/ajax\/squdra\/allenatore\/statistiche\/stagione"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "news_get_team_allenatore_page": {
            "tokens": [["variable", "\/", "[^\/]++", "codgioc"], ["text", "\/pp\/ajax\/squdra\/allenatore\/correlate"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "biglietteria.puntivendita.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/puntivendita"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "biglietteria.puntivendita_geojson.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/puntivendita_geojson"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "biglietteria.datipuntivendita.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/datipuntovendita"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "biglietteria.datipuntivendita_json.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/datipuntovendita_json"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "biglietteria.selezionaprovincia.ajax": {
            "tokens": [["text", "\/pp\/ajax\/bu_selprov"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "biglietteria.puntivenditaprovincia.ajax": {
            "tokens": [["text", "\/pp\/ajax\/bu_pdv"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "biglietteria.acquistabigliettichampions.ajax": {
            "tokens": [["text", "\/pp\/ajax\/abbonamenti\/acquista_biglietti_champions"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "biglietteria.acquistabigliettiavvertenze.ajax": {
            "tokens": [["text", "\/pp\/ajax\/abbonamenti\/acquista_biglietti_avvertenze"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "biglietteria.acquista_prelazione_derby.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/acquista_prelazione_derby"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "biglietteria.acquista_prelazione_abbonati.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/acquista_prelazione_abbonati"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "biglietteria.acquista_prelazione_abbonatiprimoanello.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/acquista_prelazione_abbonatiprimoanello"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "biglietteria.acquista_prelazione_tdt.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/acquista_prelazione_tdt"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "biglietteria.acquista_prelazione_champions.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/acquista_prelazione_champions"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "biglietteria.acquista_tdt_champions.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/acquista_tdt_champions"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "biglietteria.acquista_prelazione_abbonatinonsecondoanelloverde.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/acquista_prelazione_abbonatinonsecondoanelloverde"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "news.get_evidence_and_related.ajax": {
            "tokens": [["text", "\/pp\/ajax\/common\/block\/news-evidence-related"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "interisti.weareinter_new.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interisti\/weareinter_new"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interisti.daticlub.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interisti\/daticlub"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interisti.datitifoso.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interisti\/datitifoso"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interisti.listatifosi.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interisti\/listatifosi"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "blackandblue.ajax": {
            "tokens": [["text", "\/blackandbluefriday"], ["variable", "\/", "[^\/]++", "lang"], ["text", "\/pp\/ajax"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "verify.tdt.ajax": {
            "tokens": [["text", "\/verify-tdt"], ["variable", "\/", "[^\/]++", "lang"], ["text", "\/pp\/ajax"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "dem-email-layout": {
            "tokens": [["text", "\/email\/dem-layout"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "news_get_team_giocatore_page": {
            "tokens": [["variable", "\/", "[^\/]++", "codgioc"], ["text", "\/pp\/ajax\/squadra\/giocatore\/correlate"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "news_get_team_giocatore_dettagli": {
            "tokens": [["text", "\/pp\/ajax\/squadra\/giocatore\/dettagli"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "team_giocatore_get_statistiche": {
            "tokens": [["text", "\/pp\/ajax\/squadra\/giocatore\/statistiche"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interacademy.get_news.ajax": {
            "tokens": [["text", "\/pp\/ajax\/inter-academy\/news"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interforever.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interforever"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interforever_players.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interforever\/players"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interforever_players_scheda.ajax": {
            "tokens": [["text", "\/pp\/ajax\/inter_forever\/aprischeda"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interforever_players_birthdays.ajax": {
            "tokens": [["text", "\/pp\/ajax\/inter_forever\/getBirthdays"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "inter_in_the_community.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interinthecommunity"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "inter_in_the_community_news.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interinthecommunity-news"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interchannel.get_news.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interchannel\/news"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interchannel.get_videos.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interchannel\/videos"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interclub.coupon_check.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/coupon_check"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "interclub.membership_check.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/membership_check"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "interclub.get_coordinators.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/coordinators"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interclub.get_news.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/news"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interclub.get_top_news.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/topnews"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interclub.get_videos.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/videos"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interclub.get_experience_videos.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/experience-videos"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interclub.get_records.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/record"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interisti.club_mappa.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interisti\/club_mappa1516"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interisti.infoclub_mappa.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interisti\/infoclub_mappa1516"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interisti.infoclub_autosuggest.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interisti\/infoclub_autosuggest1617"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interisti.interclub_autosuggest.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclubs\/suggest"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interisti.interclub_list_by_region.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclubs\/list_by_region"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interisti.geocodifica_club.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interisti\/geocodifica_club"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interclub.contact_request.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/message"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "interclub.contact_request_coordinator.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/coordinator\/message"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "interclub.coordination_contact_request.ajax": {
            "tokens": [["text", "\/pp\/ajax\/interclub\/coordination\/message"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "interclub.cover_news": {
            "tokens": [["text", "\/pp\/interclub\/cover-news"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interclub.becomememberdata": {
            "tokens": [["text", "\/pp\/interclub\/becomememberdata"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "intertv.get-live-show.ajax": {
            "tokens": [["text", "\/pp\/ajax\/intertv\/get-live-show"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "registration.countries": {
            "tokens": [["text", "\/pp\/ajax\/localization\/countries"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "registration.regions": {
            "tokens": [["text", "\/pp\/ajax\/localization\/regions"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "registration.municipalities": {
            "tokens": [["text", "\/municipalities"], ["variable", "\/", "\\w{2}", "region"], ["text", "\/pp\/ajax\/localization\/regions"]],
            "defaults": [],
            "requirements": {"region": "\\w{2}", "_method": "GET"},
            "hosttokens": []
        },
        "localization.common": {
            "tokens": [["variable", "\/", "it|en|es|cn|jp|ba", "lang"], ["text", "\/pp\/ajax\/localization\/common"]],
            "defaults": [],
            "requirements": {"lang": "it|en|es|cn|jp|ba", "_method": "GET"},
            "hosttokens": []
        },
        "match_center_vota_pagelle": {
            "tokens": [["text", "\/match_center\/vota"], ["variable", "\/", "[^\/]++", "lang"], ["text", "\/pp"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "news_get_news_evidence_with_filters": {
            "tokens": [["variable", "\/", "[^\/]++", "idcategoria"], ["text", "\/pp\/ajax\/news\/news-get-evidence-with-filters"]],
            "defaults": {"idcategoria": 0},
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "news_get_news_single": {
            "tokens": [["variable", "\/", "[^\/]++", "idnews"], ["text", "\/news\/news_single"]],
            "defaults": {"idnews": 0},
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "news_get_search_results": {
            "tokens": [["variable", "\/", "[^\/]++", "categoria"], ["variable", "\/", "[^\/]++", "data"], ["variable", "\/", "[^\/]++", "termine"], ["text", "\/pp\/ajax\/news\/news-get-search_results"]],
            "defaults": {"termine": "", "data": 0, "categoria": 0},
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "news_get_darwin_movie.ajax": {
            "tokens": [["text", "\/pp\/ajax\/darwin-movie"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "squadra_get_team_playerslist": {
            "tokens": [["text", "\/pp\/ajax\/squdra\/squadra_team_playerslist"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "squadra_get_team_stats": {
            "tokens": [["text", "\/pp\/ajax\/squdra\/squadra_team_stats"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "squadra_season_stats": {
            "tokens": [["text", "\/pp\/ajax\/squadra\/season_stats"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "squadra_player_photo": {
            "tokens": [["text", "\/pp\/ajax\/squadra\/player\/photo"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "contattaci_get_motivi": {
            "tokens": [["text", "\/pp\/ajax\/contattaci\/motivi"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "contattaci_get_motivi_dettagli": {
            "tokens": [["text", "\/pp\/ajax\/contattaci\/motivo"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "registration.save": {
            "tokens": [["variable", "\/", ".*", "lang"], ["text", "\/pp\/ajax\/registration"]],
            "defaults": [],
            "requirements": {"lang": ".*", "_method": "POST"},
            "hosttokens": []
        },
        "user_profile": {
            "tokens": [["text", "\/pp\/ajax\/profile"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "rimozione_utente.ajax": {
            "tokens": [["text", "\/user-remove"], ["variable", "\/", ".*", "lang"], ["text", "\/pp"]],
            "defaults": [],
            "requirements": {"lang": ".*", "hashcod": ".*", "_method": "POST"},
            "hosttokens": []
        },
        "registrazione.unbind_social.ajax": {
            "tokens": [["text", "\/pp\/ajax\/user-unbind-social"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "recupero_pwd.ajax": {
            "tokens": [["text", "\/passreminder"], ["variable", "\/", ".*", "lang"], ["text", "\/pp\/ajax"]],
            "defaults": [],
            "requirements": {"lang": ".*", "_method": "POST"},
            "hosttokens": []
        },
        "disassocia_tessera.ajax": {
            "tokens": [["text", "\/pp\/ajax\/tdt\/disassocia"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "check_tdt.ajax": {
            "tokens": [["text", "\/pp\/ajax\/tdt\/checkTdt"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "check_esistenza_tdt.ajax": {
            "tokens": [["text", "\/pp\/ajax\/tdt\/checkEsistenzaTdt"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "user_consents": {
            "tokens": [["text", "\/pp\/ajax\/user_consents"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "screamer.next_match.button": {
            "tokens": [["text", "\/pp\/screamer\/next-match\/button"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "login.ajax": {
            "tokens": [["text", "\/pp\/ajax\/login"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "logout.ajax": {
            "tokens": [["text", "\/pp\/ajax\/logout"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "session_start.ajax": {
            "tokens": [["text", "\/pp\/ajax\/session_start"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "account_verify.ajax": {
            "tokens": [["text", "\/pp\/ajax\/account_verify"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "login_items.ajax": {
            "tokens": [["text", "\/pp\/ajax\/login_items"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "autologin_app.ajax": {
            "tokens": [["text", "\/pp\/ajax\/autologin-app"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "serviziabbonamenti.get_punti_vendita_speciali.ajax": {
            "tokens": [["text", "\/pp\/ajax\/biglietteria\/puntivenditaspeciali"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "serviziabbonamenti.scuolecalcio_partite.ajax": {
            "tokens": [["text", "\/pp\/ajax\/scuolecalcio_partite"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "serviziabbonamenti.scuolecalcio_salva.ajax": {
            "tokens": [["text", "\/pp\/ajax\/scuolecalcio_salva"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "settoregiovanile.get_news.ajax": {
            "tokens": [["text", "\/pp\/ajax\/settoregiovanile\/news"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "settoregiovanile.get_teams.ajax": {
            "tokens": [["text", "\/pp\/ajax\/settoregiovanile\/teams"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "settoregiovanile.get_videos.ajax": {
            "tokens": [["text", "\/pp\/ajax\/settoregiovanile\/videos"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "settoregiovanile.settore_giovanile.ajax": {
            "tokens": [["text", "\/pp\/ajax\/giovanili\/settore-giovanile-youth-sector"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "settoregiovanile.training_centers.ajax": {
            "tokens": [["text", "\/pp\/ajax\/giovanili\/training_centers"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "settoregiovanile.training_center.contact.ajax": {
            "tokens": [["text", "\/pp\/ajax\/giovanili\/training_center\/contact"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "settoregiovanile.livescoreticker": {
            "tokens": [["text", "\/giovanili\/live-score-ticker"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "settoregiovanile.primaveralive": {
            "tokens": [["variable", "", "\\d{2}", "id"], ["variable", "-", "[^\/]+", "rand"], ["text", "\/primavera-inter-live"]],
            "defaults": [],
            "requirements": {"random": "\\w+", "id": "\\d{2}", "_method": "GET"},
            "hosttokens": []
        },
        "social.instagram.feed.ajax": {
            "tokens": [["text", "\/pp\/ajax\/instagram\/feed"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "trasferte.dati_utente.ajax": {
            "tokens": [["text", "\/pp\/ajax\/trasferte_datiutente"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "trasferte.log.ajax": {
            "tokens": [["text", "\/pp\/ajax\/trasferte_log"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "trasferte.salva.ajax": {
            "tokens": [["text", "\/pp\/ajax\/trasferte_salva"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "userarea.get_esclusivo_page.ajax": {
            "tokens": [["variable", "\/", "[^\/]++", "lang"], ["text", "\/pp\/ajax\/esclusivo\/videos"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "userarea.get_esclusivo_video.ajax": {
            "tokens": [["variable", "\/", "[^\/]++", "id"], ["variable", "\/", "[^\/]++", "lang"], ["text", "\/pp\/ajax\/esclusivo\/videos"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "userarea.get_intermagazine_page.ajax": {
            "tokens": [["variable", "\/", "[^\/]++", "lang"], ["text", "\/pp\/ajax\/intermagazine\/page"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "userarea.get_intermagazine_post.ajax": {
            "tokens": [["variable", "\/", "[^\/]++", "id"], ["variable", "\/", "[^\/]++", "lang"], ["text", "\/pp\/ajax\/intermagazine\/read"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "userarea.get_full_intermagazine.ajax": {
            "tokens": [["text", "\/pp\/ajax\/intermagazine\/read"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "visorehp.get-data-for-match.ajax": {
            "tokens": [["text", "\/pp\/ajax\/get-data-for-match"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "derby.seat.ajax": {
            "tokens": [["text", "\/pp\/ajax\/derby\/seat"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "derby.seat_email.ajax": {
            "tokens": [["text", "\/pp\/ajax\/derby\/seat\/email"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "interutils_calendario.ajax": {
            "tokens": [["text", "\/pp\/ajax\/inter_utils\/calendario"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interutilsslides-speciale.ajax": {
            "tokens": [["text", "\/pp\/ajax\/inter_utils\/slides-speciale"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "interutils-rss-instant-articles.ajax": {
            "tokens": [["text", "\/pp\/ajax\/rss\/instant-articles"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_nomination.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame-nomination"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_nomination_show.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame-nomination-show"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_nomination_awards.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame-nomination-awards"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_players.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame-players"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_vote.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame-vote"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_player.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame-player"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_stats.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame-stats"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_stats_full.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame-stats-full"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_vote_by_day.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame-stats-vote-by-day"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_insert_log.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/insert-log"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_canale.ajax": {
            "tokens": [["text", "\/pp\/ajax\/societaclub\/halloffame-stats-canale"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_hall_of_fame_get_votes": {
            "tokens": [["text", ".jpg"], ["variable", "\/", "[^\/\\.]++", "hash"], ["text", "\/societa_club\/halloffame-vote"], ["variable", "\/", "[^\/]++", "lang"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "societaclub_stay_with_us_modal": {
            "tokens": [["text", "\/pp\/stay-with-us\/modal"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "get_partner_consents": {
            "tokens": [["text", "\/pp\/ajax\/partner_consents"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "save_partner_consents": {
            "tokens": [["text", "\/pp\/ajax\/partner_consents"]],
            "defaults": [],
            "requirements": {"_method": "POST"},
            "hosttokens": []
        },
        "get_partner_geolocation": {
            "tokens": [["variable", "\/", "[^\/]++", "country"], ["text", "\/pp\/geopartner"]],
            "defaults": {"country": null},
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "competition_partner": {
            "tokens": [["text", "\/pp\/competition-geopartner"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "user.profile.upgrade": {
            "tokens": [["text", "\/pp\/ajax\/user-profile-upgrade"]],
            "defaults": [],
            "requirements": {"_method": "GET"},
            "hosttokens": []
        },
        "inter_site_router": {
            "tokens": [["variable", "\/", ".*", "route"]],
            "defaults": [],
            "requirements": {"route": ".*", "_method": "GET|POST"},
            "hosttokens": []
        }
    },
    "prefix": "",
    "host": "www.inter.it",
    "scheme": "https"
});